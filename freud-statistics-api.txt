// METHOD NAME
name_len: uint32_t
fname: sizeof(char) * name_len

// FEATURE NAMES
feature_names_count: uint32_t
    vnlen: uint16_t
    vname: sizeof(char) * vnlen

// TYPE NAMES
type_names_count: uint32_t
    vnlen: uint16_t
    tname: sizeof(char) * vnlen

// SAMPLES
samples_count: uint32_t
    uid_r: uint32_t
    time: uint64_t
	mem: uint64_t
    lock_holding_time: uint64_t
	waiting_time: uint64_t
    minor_page_faults: uint64_t
	major_page_faults: uint64_t

    // FEATURES
    num_of_features: uint32_t
        name_offset: uint64_t
        type_offset: uint64_t
        value: int64_t

    // BRANCHES
    num_of_branches: uint32_t
        branch_id: uint16_t
        num_of_executions: uint32_t
            taken: bool

    // CHILDREN
    num_of_children: uint32_t
        c_id: uint32_t
